<?php
function bottom() {
	echo <<<EOT
		<div id="footer">
			<div id="footer-center">
				<div id="other-calculators">
					<h5>OTHER CALCULATORS</h5>
					<ul>
						<li><a href="/average/">Average</a></li>
						<li><a href="/basic/">Basic</a></li>
						<li><a href="/congruence/">Congruence</a></li>
						<li><a href="/gcd/">GCD</a></li>
						<li><a href="/lcm/">LCM</a></li>
						<li><a href="/logarithm/">Logarithm</a></li>
					</ul>
					<ul>
						<li><a href="/mortgage/">Mortgage</a></li>
						<li><a href="/surd/">Surd Simplifier</a></li>
						<li><a href="/words/">Words Converter</a></li>
					</ul>
					<ul>
						<li>&nbsp;</li>
					</ul>
					<ul class="last">
						<li>&nbsp;</li>
					</ul>
				</div>
				<div id="information">
					<h5>INFORMATION</h5>
					<ul class="last">
						<li>
							<a href="http://docs.google.com/forms/d/1usJRVRmpopb564GXD-MvfyMfK0bap4GCoGLtyv4Wvz8/viewform" rel="nofollow">Feedback</a>
						</li>
						<li>
							<a href="/terms.html#privacy">Privacy</a>
						</li>
						<li>
							<a href="/terms.html#terms">Terms</a>
						</li>
					</ul>
				</div>
				<hr />
				<p>&copy; 2012-2013 a-calculator.com &ndash; All rights reserved. All wrongs avenged.</p>
			</div>
		</div>
	</body>
</html>
EOT;
}
