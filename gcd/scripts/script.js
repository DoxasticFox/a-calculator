/* Greatest common divisor */
function gcd(a, b) {
	if (b == 0)
		return Math.abs(a);
	return Math.abs(gcd(b, a%b));
}


/* Returns array of solutions to congruence equation
   ax = b (mod m). Solutions are sorted */
function solve (input) {
	/* Base cases */
	if (input.length == 0) return 0;
	if (input.length == 1) return input[0];
	if (input.length == 2) return gcd(input[0], input[1]);
	
	/* Recursive call */
	var head = input[0];
	var tail = input.slice(1);
	return gcd(head, solve(tail));
}

/*
	HANDLING THE FRONT-END USING JQUERY
*/
var firstClick = true;
$(document).ready(function() {
	$("input[type=button]").click(function() {
		calculate();
		firstClick = false;
	});
	bindKeyPresses();
	$("#gcd-container li input").click(function() {
		if (firstClick === true) {
			$("#gcd-container li input").val("");
			$("#result").html("GCD: 0");
			firstClick = false;
		}
	});
	$("#add").click(function() {
		$("#gcd-container ol").append('<li><input type="text"/></li>');
		bindKeyPresses();
		firstClick = false;
	});
	$("#remove").click(function() {
		if ($("#gcd-container li input").length > 1)
			$("#gcd-container li").last().remove();
		firstClick = false;
	});
});

/* Causes the input boxes to respond appropriately when
   input is typed into them */
function bindKeyPresses () {
	$("#gcd-container li input").keypress(function(e) {
		if(e.which == 13)
			calculate();
	});
	$("#gcd-container li input").keyup(function() {
		validateInput();
	});
}

/* What do do when the user clicks the "calculate"
   button. Validates input and displays a result or
   error. */
function calculate () {
	var input = getInput();
	var valid = validateInput();
	
	if (!valid)
		setOutput(valid);
	else
		setOutput(solve(input));
}

/* Gets the raw values from the input boxes */
function getInput () {
	var input = [];
	$("#gcd-container li input").each(function(index) {
        var inputBoxContents = $(this).val();
		input.push(inputBoxContents);
    });
	return input;
}

/* Styles the input boxes to indicate whether or not
   the input is valid. Returns true if the input is
   valid, false otherwise. */
function validateInput () {
	var isValid = true;
	
	$("#gcd-container li input").each(function(index) {
        $(this).removeClass("invalid");
    });
	
	$("#gcd-container li input").each(function(index) {
        var inputBoxContents = $(this).val();
		if (/[^0-9]/.test(inputBoxContents)) {
			$(this).addClass("invalid");
			isValid = false;
		}
    });
	
	return isValid;
}

function setOutput (solution) {
	if (solution === false) {
		$("#result").html("The values you enter must be integers.");
		return;
	}
	
	$("#result").html("GCD: " + solution);
}