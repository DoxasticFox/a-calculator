<?php include("../include/sections.php") ?>
<?php top("Greatest Common Divisor Calculator", "Finds the largest positive integer which divides two (or more) numbers without a remainder.") ?>
		<div id="gcd-container">
			<div id="gcd">
				<div class="pane">
					<table border="0" width="100%">
						<tr>
							<td width="40%">
								<ol type="a">
									<li><input type="text" value="3" /></li>
									<li><input type="text" value="6" /></li>
								</ol>
							</td>
							<td id="result">GCD: 3</td>
						</tr>
					</table>
					<a href="#" id="add"><img src="/images/green-cross.png" alt="Add an extra integer" />Add</a>
					<a href="#" id="remove"><img src="/images/red-cross-rotate-45.png" alt="Remove the last integer" />Remove</a>
					
				</div>
				
				<div class="button-pane">
					<input type="button" name="calculate" value="Calculate" />
					<div class="branding">A-CALCULATOR.COM</div>
				</div>
			</div>
		</div>
<?php middle() ?>
		<h2>FAQs &amp; How-to's</h2>
		<div id="page-text">
			<h3>About This Calculator</h3>
			<!-- FAQ Questions -->
			<ol>
				<li>
					<a href="#faq-whatis">What is this calculator for?</a>
				</li>
				<li>
					<a href="#faq-use">Can I embed this on my website?</a>
				</li>
				<li>
					<a href="#faq-formula">How do I find the greatest common divisor manually?</a>
				</li>
			</ol>
			<br />
			
			<!-- FAQ Answer -->
			<h4 id="faq-whatis">
				What is this calculator for?
			</h4>
			<p>
				This calculator finds the greatest common divisor (GCD) of a set of integers. It can be used to calculate it for two or more numbers.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-use">
				Can I embed this on my website?
			</h4>
			<p>
				Sure. Embedding is allowed as long as you promise to follow <a href="/terms.html#embed">our conditions</a>. Here's the embed code:
			</p>
			<code id="embed">
				&lt;iframe width=&quot;415&quot; height=&quot;220&quot; src=&quot;http://www.a-calculator.com/gcd/embed.html&quot; frameborder=&quot;0&quot; allowtransparency=&quot;true&quot;&gt;&lt;/iframe&gt;
			</code>
			
			<!-- FAQ Answer -->
			<h4 id="faq-formula">
				How do I find the greatest common divisor manually?
			</h4>
			<p>
				To get an idea about what the GCD really is, let's go through the steps of finding it for 3 and 6. One way to do so would be to list the divisors of each number like this:
				<ul>
					<li>For 3 they are 1 and 3</li>
					<li>And for 6 they are 1, 3 and 6</li>
				</ul>
				Now we can see that 3 and 6 are both evenly divided by 1 and 3 but 3 is the highest. Therefore 3 is the GCD 3 and 6.
			</p>
			<p>
				Another common technique is the Euclidean algorithm, which can be stated recursively as
				\( \def\myfunc{\gcd} \)
				\begin{equation}
					\myfunc(a, b) =
						\begin{cases}
							a                            & \text{if } b = 0, \\
							\myfunc(b, a \text{ modulo } b) & \text{otherwise}.
						\end{cases}
				\end{equation}
				To use it for three numbers \(a, b\) and \(c\) you can simply write \(\myfunc(a, \myfunc(b, c))\).
			</p>
		</div>
<?php bottom() ?>
