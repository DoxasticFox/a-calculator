function solve (input) {
	var factors = [];
	factors['integers'] = [];
	factors['surd'] = [];

	if (isInteger(Math.sqrt(input))) {
		factors['integers'][Math.sqrt(input)] = 1;
		return factors;
	}
	
	var dividend = input;
	var lsd = lowestSquareDivisor(dividend);
	while (lsd != 1) {	
		if (!isFinite(factors['integers'][lsd]))
			factors['integers'][lsd] = 0;
		factors['integers'][lsd]++;
		
		dividend /= lsd*lsd;
		lsd = lowestSquareDivisor(dividend);
	}
	
	factors['surd'].push(dividend);
	return factors;
}

/*
	HANDLING THE FRONT-END USING JQUERY
*/
var firstClick = true;
$( document ).ready(function() {
	$("#factors").change(function() {
		calculate(this.checked);
		firstClick = false;
	});
	$("input[type=button]").click(function() {
		calculate($("#factors").prop("checked"));
		firstClick = false;
	});
	$("input[name=input]").keypress(function(e) {
		if(e.which == 13)
			calculate($("#factors").prop("checked"));
	});
	$("input[name=input]").keyup(function() {
		validateInput(getInput());
	});
	$("input[name=input]").click(function() {
		if (firstClick === true) {
			$("input[name=input]").val("");
			$("input[name=output]").val("");
			firstClick = false;
		}
	});
});

function calculate (showFactors) {
	var input = getInput();
	var valid = validateInput(input);
	
	if (!valid) {
		setOutput(valid);
		return;
	}
	
	var solution = solve(input);
	var integers = solution['integers'];
	var surd = solution['surd'];
	var output = "";
	if (showFactors === false) {
		var evaluation = evaluateFactors(solution);
		if (evaluation > 1) {
			var integers_ = [];
			integers_[evaluation] = 1;
			integers = integers_;
		}
	}
	for (var key in integers) {
		output += key;
		if (integers[key] > 1)
			output += "<sup>" + integers[key] + "</sup>";
		output += "&times;";
	}
	if (surd.length == 0 || showFactors === false)
		output = output.substring(0, output.length - "&times;".length);
	for (var key in surd) {
		output += "&radic;<span class=\"overline\">" + surd[key] + "</span>";
	}
	
	setOutput(output);
}

function getInput () {
	return $("input[name=input]").val();
}

/* Styles the input boxes to indicate whether or not
   the input is valid. */
function validateInput (input) {
	var isValid = true;
	
	$("input[name=input]").removeClass("invalid");
	
	if (/[^0-9]/.test(input)) {
		$("input[name=input]").addClass("invalid");
		isValid = false;
	}
	
	return isValid;
}

function setOutput (output) {
	if (output === false)
		$("#output").html("Invalid input");
	else
		$("#output").html(output);
}

/* Helper functions */
function floatDivisible(float_, divisor) {
	var epsilon = 1e-50;
	
	if (Math.ceil(float_) % divisor < epsilon)
		return true;
	return false;
}

function isInteger (float_) {
	var ceil = Math.ceil(float_);
	
	if (floatsEqual(float_, ceil))
		return true;
	return false;
}

function floatsEqual (f1, f2) {
	var epsilon = 1e-50;
	
	if (Math.abs(f1-f2) < epsilon)
		return true;
	return false;
}

function lowestSquareDivisor (n) {
	var divisor = 2;
	var divisorSquared = divisor*divisor;
	
	if (floatDivisible(n, divisorSquared))
		return Math.round(divisor);
	divisor++;
	divisorSquared = divisor*divisor;
	
	while (divisorSquared < n) {
		if (floatDivisible(n, divisorSquared))
			return Math.round(divisor);
		divisor += 2;
		divisorSquared = divisor*divisor;
	}
	
	return 1;
}

function evaluateFactors (factors) {
	var integers = factors['integers'];
	var result = 1;
	for (var key in integers) {
		result *= Math.pow(key, integers[key]);
	}
	return result;
}
