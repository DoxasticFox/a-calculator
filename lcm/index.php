<?php include("../include/sections.php") ?>
<?php top("Least Common Multiple Calculator", "Finds the least common multiple of two (or more) integers.") ?>
		<div id="lcm-container">
			<div id="lcm">
				<div class="pane">
					<table border="0" width="100%">
						<tr>
							<td width="40%">
								<ol type="a">
									<li><input type="text" value="4" /></li>
									<li><input type="text" value="6" /></li>
								</ol>
							</td>
							<td id="result">LCM: 12</td>
						</tr>
					</table>
					<a href="#" id="remove"><img src="/images/red-cross-rotate-45.png" alt="Remove the last integer" />Remove</a>
					<a href="#" id="add"><img src="/images/green-cross.png" alt="Add an extra integer" />Add</a>
				</div>
				
				<div class="button-pane">
					<input type="button" name="calculate" value="Calculate" />
					<div class="branding">A-CALCULATOR.COM</div>
				</div>
			</div>
		</div>
<?php middle() ?>
		<h2>FAQs &amp; How-to's</h2>
		<div id="page-text">
			<h3>About This Calculator</h3>
			<!-- FAQ Questions -->
			<ol>
				<li>
					<a href="#faq-whatis">What is this calculator for?</a>
				</li>
				<li>
					<a href="#faq-use">Can I embed this on my website?</a>
				</li>
				<li>
					<a href="#faq-formula">How do I find the least common multiple by hand?</a>
				</li>
			</ol>
			<br />
			
			<!-- FAQ Answer -->
			<h4 id="faq-whatis">
				What is this calculator for?
			</h4>
			<p>
				This calculator finds the least common multiple (LCM) of a set of integers. It can be used to find it for two or more numbers.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-use">
				Can I embed this on my website?
			</h4>
			<p>
				Sure. Embedding is allowed as long as you promise to follow <a href="/terms.html#embed">our conditions</a>. Here's the embed code:
			</p>
			<code id="embed">
				&lt;iframe width=&quot;415&quot; height=&quot;220&quot; src=&quot;http://www.a-calculator.com/lcm/embed.html&quot; frameborder=&quot;0&quot; allowtransparency=&quot;true&quot;&gt;&lt;/iframe&gt;
			</code>
			
			<!-- FAQ Answer -->
			<h4 id="faq-formula">
				How do I find the least common multiple by hand?
			</h4>
			<p>
				Here's an example. To find the least common multiple of 4 and 6 we could list the multiples of each of them:
				<ul>
					<li>The multiples of 4 are 4, 8, 12, 16, 20, 24, 28, 32, 36, 40...</li>
					<li>The multiples of 6 are 6, 12, 18, 24, 30, 36...</li>
				</ul>
				Now we can see that 4 and 6 have the divisors 12, 24, 36 (and so on) in common, but 12 is the lowest. Therefore the least common multiple of 4 and 6 is 12.
			</p>
			<p>
				Another common technique is to use the <a href="/gcd/">greatest common divisor</a>
				\( \def\myfunc{\text{lcm}} \)
				\begin{equation}
					\myfunc(a, b) = \frac{|a \times b|}{\gcd(a, b)}.
				\end{equation}
				To apply it to three numbers \(a, b\) and \(c\) you can simply use \(\myfunc(a, \myfunc(b, c))\).
			</p>
		</div>
<?php bottom() ?>
