/* Greatest common divisor */
function gcd(a, b) {
	if (b == 0)
		return Math.abs(a);
	return Math.abs(gcd(b, a%b));
}

/* Least common multiple */
function lcm (a, b) {
	return Math.abs(a*b)/gcd(a, b);
}

/* Returns array of solutions to congruence equation
   ax = b (mod m). Solutions are sorted */
function solve (input) {
	/* Base cases */
	if (input.length == 0) return 0;
	if (input.length == 1) return 1;
	if (input.length == 2) return lcm(input[0], input[1]);
	
	/* Recursive call */
	var head = input[0];
	var tail = input.slice(1);
	return lcm(head, solve(tail));
}

/*
	HANDLING THE FRONT-END USING JQUERY
*/
var firstClick = true;
$(document).ready(function() {
	$("input[type=button]").click(function() {
		calculate();
		firstClick = false;
	});
	bindKeyPresses();
	$("#lcm-container li input").click(function() {
		if (firstClick === true) {
			$("#lcm-container li input").val("");
			$("#result").html("GCD: 0");
			firstClick = false;
		}
	});
	$("#add").click(function() {
		$("#lcm-container ol").append('<li><input type="text"/></li>');
		bindKeyPresses();
		firstClick = false;
	});
	$("#remove").click(function() {
		if ($("#lcm-container li input").length > 2)
			$("#lcm-container li").last().remove();
		firstClick = false;
	});
});

/* Causes the input boxes to respond appropriately when
   input is typed into them */
function bindKeyPresses () {
	$("#lcm-container li input").keypress(function(e) {
		if(e.which == 13)
			calculate();
	});
	$("#lcm-container li input").keyup(function() {
		validateInput();
	});
}

/* What do do when the user clicks the "calculate"
   button. Validates input and displays a result or
   error. */
function calculate () {
	var input = getInput();
	var valid = validateInput();
	
	if (!valid) {
		setOutput(valid);
	} else {
		input = input.remove("");
		setOutput(solve(input));
	}
}

/* Gets the raw values from the input boxes */
function getInput () {
	var input = [];
	$("#lcm-container li input").each(function(index) {
        var inputBoxContents = $(this).val();
		input.push(inputBoxContents);
    });
	return input;
}

/* Styles the input boxes to indicate whether or not
   the input is valid. Returns true if the input is
   valid, false otherwise. */
function validateInput () {
	var isValid = true;
	
	$("#lcm-container li input").each(function(index) {
        $(this).removeClass("invalid");
    });
	
	$("#lcm-container li input").each(function(index) {
        var inputBoxContents = $(this).val();
		if (/[^0-9]/.test(inputBoxContents)) {
			$(this).addClass("invalid");
			isValid = false;
		}
    });
	
	return isValid;
}

function setOutput (solution) {
	if (solution === false) {
		var msg = "The values you enter must be integers.";
		$("#result").html(msg);
		return;
	}
	
	$("#result").html("LCM: " + solution);
}

/* UTILITY FUNCTIONS */

Array.prototype.remove = function(needle) {
	var haystack = this;
	for (var i = haystack.length-1; i >= 0; i--)
		if (haystack[i] === needle)
			haystack.splice(i, 1);
	return haystack;
}