<?php include("../include/sections.php") ?>
<?php top("Linear Congruence Solver", "A tool for solving linear congruences of the form ax &#8801; b (mod m).") ?>
		<div id="congruence-container">
			<div id="congruence">
				<div class="pane">
					<input type="text" name="a" value="28" />x &#8801; <input type="text" name="b" value="14" /> (mod <input type="text" name="m" value="6" />)

					<textarea rows="4" cols="34">General form of solutions: 2 + 3k.

Solutions for x less than 6: 2,5.</textarea> 
				</div>
				
				<div class="button-pane">
					<input type="button" name="calculate" value="Calculate" />
					<div class="branding">A-CALCULATOR.COM</div>
				</div>
			</div>
		</div>
<?php middle() ?>
		<h2>FAQs &amp; How-to's</h2>
		<div id="page-text">
			<h3>About This Calculator</h3>
			<!-- FAQ Questions -->
			<ol>
				<li>
					<a href="#faq-whatis">What is this calculator for?</a>
				</li>
				<li>
					<a href="#faq-use">Can I embed this on my website?</a>
				</li>
				<li>
					<a href="#faq-formula">How do I solve a linear congruence equation manually?</a>
				</li>
			</ol>
			<br />
			
			<!-- FAQ Answer -->
			<h4 id="faq-whatis">
				What is this calculator for?
			</h4>
			<p>
				This is a linear congruence solver made for solving equations of the form \(ax \equiv b \; ( \text{mod} \; m) \), where \( a \), \( b \) and \( m \) are integers, and \( m \) is positive.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-use">
				Can I embed this on my website?
			</h4>
			<p>
				Sure. Embedding is allowed as long as you promise to follow <a href="/terms.html#embed">our conditions</a>. Here's the embed code:
			</p>
			<code id="embed">
				&lt;iframe width=&quot;415&quot; height=&quot;220&quot; src=&quot;http://www.a-calculator.com/congruence/embed.html&quot; frameborder=&quot;0&quot; allowtransparency=&quot;true&quot;&gt;&lt;/iframe&gt;
			</code>
			
			<!-- FAQ Answer -->
			<h4 id="faq-formula">
				How do I solve a linear congruence equation manually?
			</h4>
			<p>
				The calculations are somewhat involved. In an equation \(ax \equiv b \; ( \text{mod} \; m) \) the first step is to reduce \( a \) and \( b \) mod \( m \). For example, if we start off with \( a = 28 \), \( b = 14\) and \( m = 6 \) the reduced equation would have \( a = 4\) and \( b = 2\).
			</p>
			<p>
				Next we use the extended Euclidean algorithm to find two numbers, \( p \) and \( q \) such that
				\begin{equation*}
					ap + mq = \text{gcd}(a, m).
				\end{equation*}
				(Even though the algorithm finds both \( p \) and \( q \), we only need \( p \) for this.)
			</p>
			<p>
				Now, unless \( \text{gcd}(a, m) \) evenly divides \( b \) there won't be any solutions to the linear congruence. Though if it does, our first solution is given by
				\begin{equation*}
					x_0 = \frac{bp}{\text{gcd}(a, m)} \; ( \text{mod} \; m).
				\end{equation*}
				The remaining solutions are given by
				\begin{equation*}
					x_n = x_0 + \frac{nm}{\text{gcd}(a, m)} \; ( \text{mod} \; m) \qquad \text{for }n= 1, 2, \ldots , \text{gcd} (a, m) - 1.
				\end{equation*}
			</p>
		</div>
<?php bottom() ?>
