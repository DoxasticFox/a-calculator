/*  return array [d, a, b] such that d = gcd(p, q), ap + bq = d */
function gcd_extended (p, q) {
	if (q == 0)
		return [p, 1, 0];

	var vals = gcd_extended(q, p.mod(q));
	var d = vals[0];
	var a = vals[2];
	var b = vals[1] - vals[2]*Math.floor(p/q);
	
	return [d, a, b];
}

/* Returns true if numerator evenly divides denominator. */
function divides (numerator, denominator) {
	if (numerator.mod(denominator) > 0)
		return false;
	return true;
}

/* Returns array of solutions to congruence equation
   ax = b (mod m). Solutions are sorted */
function solve (input) {
	var m = Math.abs(input[2]);
	var a = input[0].mod(m);
	var b = input[1].mod(m);
	var result_extended = gcd_extended(a, m);
	var solutions = new Array();
	
	if (!divides(b, result_extended[0]))
		return solutions;
		
	var firstSolution = (result_extended[1]*(b/result_extended[0])).mod(m);
	for (i = 0; i < result_extended[0]; i++) {
		var otherSolution = (firstSolution + i*(m/result_extended[0])).mod(m);
		solutions.push(otherSolution);
	}
	
	return solutions.sort(function(a,b){return a-b});
}

/* Modulo "operator" which works with negative numbers.
   ('%' does not). */
Number.prototype.mod = function(n) {
	return ((this%n)+n)%n;
}

/*
	HANDLING THE FRONT-END USING JQUERY
*/
var firstClick = true;
$( document ).ready(function() {
	$("input[type=button]").click(function() {
		calculate();
	});
	$("input[name=a], input[name=b], input[name=m]").keypress(function(e) {
		if(e.which == 13)
			calculate();
	});
	$("input[name=a], input[name=b], input[name=m]").keyup(function() {
		validateInput(getInput());
	});
	$("input[name=a], input[name=b], input[name=m]").click(function() {
		if (firstClick === true) {
			$("input[name=a]").val("");
			$("input[name=b]").val("");
			$("input[name=m]").val("");
			$("textarea").val("");
			firstClick = false;
		}
	});
});

function calculate () {
	var input = getInput();
	var valid = validateInput(input);
	
	if (!valid)
		setOutput(valid);
	else
		setOutput(solve(input));
}

function getInput () {
	var a = $("input[name=a]").val();
	var b = $("input[name=b]").val();
	var m = $("input[name=m]").val();
	
	return [Number(a), Number(b), Number(m)];
}

/* Styles the input boxes to indicate whether or not
   the input is valid. Expects input to be an array
   of the form [a, b, m]. */
function validateInput (input) {
	var isValid = true;
	
	$("input[name=a]").removeClass("invalid");
	$("input[name=b]").removeClass("invalid");
	$("input[name=m]").removeClass("invalid");
	
	if (/[^0-9]/.test(input[0])) {
		$("input[name=a]").addClass("invalid");
		isValid = false;
	}
	if (/[^0-9]/.test(input[1])) {
		$("input[name=b]").addClass("invalid");
		isValid = false;
	}
	if (/[^0-9]/.test(input[2])) {
		$("input[name=m]").addClass("invalid");
		isValid = false;
	}
	
	return isValid;
}

function setOutput (solutions) {
	if (solutions === false) {
		$("textarea").val("You've entered invalid values for a, b, or m. Values must be integers!");
		return;
	}
	
	if (solutions.length === 0) {
		$("textarea").val("No solutions for x.");
	} else {
		var i = getInput();
		var gcd = gcd_extended(i[0], i[2])[0];
		$("textarea").val("General form of solutions: " + solutions[0]
				+ " + " + i[2]/gcd + "k.\n\n"
				+ "Solutions for x less than " + getInput()[2] + ": "
				+ solutions + ".");
	}
}
