<?php include("../include/sections.php") ?>
<?php top("Surd Simplifier", "Simplifies a surd (or radical) of the form &radic;N so that it becomes B&radic;M, where M &le; N. Shows the factors of B that were perfect squares for M.") ?>
		<div id="radical-container">
			<div id="radical">
				<div class="pane">
					&radic;(<input type="text" maxlength="15" name="input" value="1728" />)=
					<div id="output">2<sup>3</sup>&times;3&times;&radic;<span class="overline">3</span></div>
					<br/>
					<input type="checkbox" id="factors" checked="checked" />
					<label for="factors">Show factors</label>
				</div>
				
				<div class="button-pane">
					<input type="button" name="calculate" value="Calculate" />
					<div class="branding">A-CALCULATOR.COM</div>
				</div>
			</div>
		</div>
<?php middle() ?>
		<h2>FAQs &amp; How-to's</h2>
		<div id="page-text">
			<h3>About This Calculator</h3>
			<!-- FAQ Questions -->
			<ol>
				<li>
					<a href="#faq-whatis">What is this calculator for?</a>
				</li>
				<li>
					<a href="#faq-use">Can I embed this on my website?</a>
				</li>
				<li>
					<a href="#faq-formula">How can I simplify radical expressions by hand?</a>
				</li>
			</ol>
			<br />
			
			<!-- FAQ Answer -->
			<h4 id="faq-whatis">
				What is this calculator for?
			</h4>
			<p>
				This calculator simplifies a surd so that the number below the square root sign doesn't have any perfect squares as factors.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-use">
				Can I embed this on my website?
			</h4>
			<p>
				Sure. Embedding is allowed as long as you promise to follow <a href="/terms.html#embed">our conditions</a>. Here's the embed code:
			</p>
			<code id="embed">
				&lt;iframe width=&quot;415&quot; height=&quot;135&quot; src=&quot;http://www.a-calculator.com/surd/embed.html&quot; frameborder=&quot;0&quot; allowtransparency=&quot;true&quot;&gt;&lt;/iframe&gt;
			</code>
			
			<!-- FAQ Answer -->
			<h4 id="faq-formula">
				How can I simplify radical expressions by hand?
			</h4>
			<p>
				To simplify surds you should know square numbers (\(2^2 = 4\), \(3^2 = 9\), \(4^2 = 16\), etc). Using this knowledge you can break the number under the root sign into factors that are perfect squares like so:
				\begin{equation*}
					\sqrt{12} = \sqrt{4 \times 3} = \sqrt{2^2 \times 3} = \sqrt{2^2} \times \sqrt{3} = 2 \sqrt{3}.
				\end{equation*}
			</p>
			<p>
				A surd is said to be in its simplest form when the number under the root sign has no square factors. For example \(\sqrt{72}\) can be reduced to \(\sqrt{4 \times 18} = 2 \sqrt{18}\). But \(18\) still has the factor \(9\), so we can simplify further: \(2 \sqrt{18} = 2 \sqrt{9 \times 2} = 2 \times 3 \sqrt{2} = 6\sqrt{2}\). We stop at this stage seeing that \(2\) has no square numbers as factors.
			</p>
		</div>
<?php bottom() ?>
