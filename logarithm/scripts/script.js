function solve (input) {
	return Math.log(input);
}

/*
	HANDLING THE FRONT-END USING JQUERY
*/
var firstClick = true;
$( document ).ready(function() {
	$("input[type=button]").click(function() {
		calculate();
	});
	$("input[name=input]").keypress(function(e) {
		if(e.which == 13)
			calculate();
	});
	$("input[name=input]").keyup(function() {
		validateInput(getInput());
	});
	$("input[name=input], input[name=output]").click(function() {
		if (firstClick === true) {
			$("input[name=input]").val("");
			$("input[name=output]").val("");
			firstClick = false;
		}
	});
});

function calculate () {
	var input = getInput();
	var valid = validateInput(input);
	
	if (!valid)
		setOutput(valid);
	else
		setOutput(solve(input));
}

function getInput () {
	return $("input[name=input]").val();
}

/* Styles the input boxes to indicate whether or not
   the input is valid. Expects input to be an array
   of the form [a, b, m]. */
function validateInput (input) {
	var isValid = true;
	
	$("input[name=input]").removeClass("invalid");
	
	if (/[^0-9\.]/.test(input) || countSubstr(input, "\\.") > 1) {
		$("input[name=input]").addClass("invalid");
		isValid = false;
	}
	
	return isValid;
}

function setOutput (solution) {
	if (solution === false)
		$("input[name=output]").val("No solution");
	else
		$("input[name=output]").val(solution);
}

function countSubstr (haystack, needle){
   var regExp = new RegExp(needle, "gi");
   return haystack.match(regExp) ? haystack.match(regExp).length : 0;  
}