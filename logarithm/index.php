<?php include("../include/sections.php") ?>
<?php top("Natural Logarithm Calculator", "Finds the natural logarithm of a number. Also includes additional information on the log(x) function.") ?>
		<div id="logarithm-container">
			<div id="logarithm">
				<div class="pane">
					log<sub>e</sub>(<input type="text" name="input" value="42" />)=
					<input type="text" name="output" value="3.7376696182833684" readonly="readonly" />
				</div>
				
				<div class="button-pane">
					<input type="button" name="calculate" value="Calculate" />
					<div class="branding">A-CALCULATOR.COM</div>
				</div>
			</div>
		</div>
<?php middle() ?>
		<h2>FAQs &amp; How-to's</h2>
		<div id="page-text">
			<h3>About This Calculator</h3>
			<!-- FAQ Questions -->
			<ol>
				<li>
					<a href="#faq-whatis">What is this calculator for?</a>
				</li>
				<li>
					<a href="#faq-use">Can I embed this on my website?</a>
				</li>
				<li>
					<a href="#faq-formula">How can I find the natural logarithm without a calculator?</a>
				</li>
			</ol>
			<br />
			
			<!-- FAQ Answer -->
			<h4 id="faq-whatis">
				What is this calculator for?
			</h4>
			<p>
				This calculator takes the base e logarithm of a number.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-use">
				Can I embed this on my website?
			</h4>
			<p>
				Sure. Embedding is allowed as long as you promise to follow <a href="/terms.html#embed">our conditions</a>. Here's the embed code:
			</p>
			<code id="embed">
				&lt;iframe width=&quot;415&quot; height=&quot;135&quot; src=&quot;http://www.a-calculator.com/logarithm/embed.html&quot; frameborder=&quot;0&quot; allowtransparency=&quot;true&quot;&gt;&lt;/iframe&gt;
			</code>
			
			<!-- FAQ Answer -->
			<h4 id="faq-formula">
				How can I find the natural logarithm without a calculator?
			</h4>
			<p>
				In general, there isn't an exact way to do this. Manually approximating the log of a number is hard! To make it easier, we must remember three things. Firstly, that
				\begin{equation}
					\label{series}
					\tag{1}
					\ln(x) = 2 \left( k + \frac{k^3}{3} + \frac{k^5}{5} + \ldots \right) \qquad \text{where } k = \frac{x-1}{x+1},
				\end{equation}
				secondly, 
				\begin{equation}
					\ln(10) \approx 2.30 \qquad \text{(3 significant figures)},
				\end{equation}
				and lastly we must remember our log laws!
			</p>
			<p>
				Now, suppose we want to find an approximation of \(\ln(x_0)\), then we would rewrite it as
				\begin{equation*}
					\ln(x_0) = \ln(x_0/10^n) + n \times \ln(10) \qquad \text{where } \frac{x_0}{10^n} \approx 1.
				\end{equation*}
				After rewriting it we can figure out \(\ln(x_0/10^n)\) using equation \(\eqref{series}\), and we know that \(\ln(10) \approx 2.30\). The reason we want \(x_0/10^n\) to be close to \(1\) is so that we can use fewer terms from equation \(\eqref{series}\)  to get a good approximation (faster convergence).
			</p>
			<p>
				As an example, if we want to find \(\ln(643)\) we would do the following
				\begin{align*}
					\ln(643) &= \ln(0.643 \times 10^3) \\
					&= \ln(0.643) + \ln(10^3) \\
					&= \ln(0.643) + 3 \ln(10) \\
					&\approx \ln(0.643) + 3 \times 2.3. \tag{2} \label{approx1}
				\end{align*}
			</p>
			<p>
				To get \(\ln(0.643)\) using \(\eqref{series}\), we write \(k = (0.643 - 1)/(0.643 + 1) \approx -0.217 \) so that
				\begin{align*}
					\ln(0.643) & \approx 2 \left( -0.217 + \frac{(-0.217)^3}{3} + \frac{(-0.217)^5}{5} \right) \\
					& \approx -0.441. \tag{3} \label{approx2}
				\end{align*}
				Notice that we've only had to write out the first three terms of equation \(\eqref{series}\) to get an acceptable approximation in this case.
			</p>
			<p>
				Substituting the result from equation \(\eqref{approx2}\) back into \(\eqref{approx1}\) we get
				
				\begin{align*}
					\ln(643) & \approx -0.441 + 3 \times 2.30 \\
					& \approx 6.5,
				\end{align*}
				
				which is pretty close! In fact, using the calculator on this page we get \(\ln(643) \approx 6.46614\).
			</p>
		</div>
<?php bottom() ?>
