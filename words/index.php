<?php include("../include/sections.php") ?>
<?php top("Number to Words Converter", "Converts a number into words in the US-American style.") ?>
		<div id="words-container">
			<div id="words">
				<div class="pane">
					<label>Convert This Number:</label>
					<input type="text" maxlength="306" name="input" id="input" value="1728" />
					<br />
					<label>Answer:</label>
					<textarea rows="4" cols="34" id="result">one thousand, seven hundred twenty-eight</textarea>
				</div>
				<div class="button-pane">
					<input type="button" name="calculate" value="Calculate" />
					<div class="branding">A-CALCULATOR.COM</div>
				</div>
			</div>
		</div>
<?php middle() ?>
		<h2>FAQs &amp; How-to's</h2>
		<div id="page-text">
			<h3>About This Calculator</h3>
			<!-- FAQ Questions -->
			<ol>
				<li>
					<a href="#faq-whatis">What is this calculator for?</a>
				</li>
				<li>
					<a href="#faq-use">Can I embed this on my website?</a>
				</li>
				<li>
					<a href="#faq-formula">How can I word a number manually?</a>
				</li>
			</ol>
			<br />
			
			<!-- FAQ Answer -->
			<h4 id="faq-whatis">
				What is this calculator for?
			</h4>
			<p>
				This calculator generates a short scale worded representation of a number. That is, a billion is 10<sup>9</sup> and not 10<sup>12</sup>. Numerals with up to 306 characters (over a centillion) can be worded, as well as decimals (e.g. 1.42 or &ldquo;one and forty-two hundredths&rdquo;) and negative numbers.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-use">
				Can I embed this on my website?
			</h4>
			<p>
				Sure. Embedding is allowed as long as you promise to follow <a href="/terms.html#embed">our conditions</a>. Here's the embed code:
			</p>
			<code id="embed">
				&lt;iframe width=&quot;415&quot; height=&quot;220&quot; src=&quot;http://www.a-calculator.com/words/embed.html&quot; frameborder=&quot;0&quot; allowtransparency=&quot;true&quot;&gt;&lt;/iframe&gt;
			</code>
			
			<!-- FAQ Answer -->
			<h4 id="faq-formula">
				How can I word a number manually?
			</h4>
			<p>
				The trick is to break the number up into blocks of three. We'll use 3251469 as an example. Once we break it up, it becomes 3,251,469. (Similarly, 14000 becomes 14,000 and 543 remains as it is.)
			</p>
			<p>
				Now we put each group into words individually. We simply get &ldquo;three&rdquo; from 3, &ldquo;two hundred fifty-one&rdquo; from 251 and &ldquo;four hundred sixty-nine&rdquo; from 469.
			</p>
			<p>
				To get the final answer we need to put the worded parts back together. The trick is that the second block from the right gets &ldquo;thousand&rdquo; added to it, the second gets &ldquo;million,&rdquo; the third, &ldquo;billion,&rdquo; and so forth. Going back to our example, 3 becomes &ldquo;three million,&rdquo; and 251 becomes &ldquo;two hundred fifty-one thousand.&rdquo; All together, we get &ldquo;three million, two hundred fifty-one thousand, four hundred sixty-nine.&rdquo;
			</p>
			<p>
				If the number isn't whole, like 0.42, the process is just a little bit different. Forty-two is converted as usual, but we also have to add &ldquo;tenths,&rdquo; &ldquo;hundredths,&rdquo; &ldquo;thousandths,&rdquo; or so forth depending on how many digits there are after the decimal point. In this example there are two digits (four and two), so the final answer is &ldquo;forty-two hundredths.&rdquo;
			</p>
		</div>
<?php bottom() ?>
