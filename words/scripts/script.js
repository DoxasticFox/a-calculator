var number = {
	 '0' : 'zero',
	 '1' : 'one',
	 '2' : 'two',
	 '3' : 'three',
	 '4' : 'four',
	 '5' : 'five',
	 '6' : 'six',
	 '7' : 'seven',
	 '8' : 'eight',
	 '9' : 'nine',
	'10' : 'ten',
	'11' : 'eleven',
	'12' : 'twelve',
	'13' : 'thirteen',
	'14' : 'fourteen',
	'15' : 'fifteen',
	'16' : 'sixteen',
	'17' : 'seventeen',
	'18' : 'eighteen',
	'19' : 'nineteen',
	
	'2x' : 'twenty',	//umpties
	'3x' : 'thirty',
	'4x' : 'forty',
	'5x' : 'fifty',
	'6x' : 'sixty',
	'7x' : 'seventy',
	'8x' : 'eighty',
	'9x' : 'ninety'
}
var exponentInterval = 3; // The difference between the keys in exponent
var exponent = {
		0 : '',
		3 : 'thousand',
		6 : 'million',
		9 : 'billion',
	   12 : 'trillion',
	   15 : 'quadrillion',
	   18 : 'quintillion',
	   21 : 'sextillion',
	   24 : 'septillion',
	   27 : 'octillion',
	   30 : 'nonillion',
	   33 : 'decillion',
	   36 : 'undecillion',
	   39 : 'duodecillion',
	   42 : 'tredecillion',
	   45 : 'quattuordecillion',
	   48 : 'quindecillion',
	   51 : 'sexdecillion',
	   54 : 'septendecillion',
	   57 : 'octodecillion',
	   60 : 'novemdecillion',
	   63 : 'vigintillion',
	   66 : 'unvigintillion',
	   69 : 'duovigintillion',
	   72 : 'trevigintillion',
	   75 : 'quattuorvigintillion',
	   78 : 'quinvigintillion',
	   81 : 'sexvigintillion',
	   84 : 'septenvigintillion',
	   87 : 'octovigintillion',
	   90 : 'novemvigintillion',
	   93 : 'trigintillion',
	   96 : 'untrigintillion',
	   99 : 'duotrigintillion',
	   // 100 : 'googol' - not latin name
	   // 10^googol = 1 googolplex
	  102 : 'trestrigintillion',
	  105 : 'quattuortrigintillion',
	  108 : 'quintrigintillion',
	  111 : 'sextrigintillion',
	  114 : 'septentrigintillion',
	  117 : 'octotrigintillion',
	  120 : 'novemtrigintillion',
	  123 : 'quadragintillion',
	  126 : 'unquadragintillion',
	  129 : 'duoquadragintillion',
	  132 : 'trequadragintillion',
	  135 : 'quattuorquadragintillion',
	  138 : 'quinquadragintillion',
	  141 : 'sexquadragintillion',
	  144 : 'septenquadragintillion',
	  147 : 'octoquadragintillion',
	  150 : 'novemquadragintillion',
	  153 : 'quinquagintillion',
	  156 : 'unquinquagintillion',
	  159 : 'duoquinquagintillion',
	  162 : 'trequinquagintillion',
	  165 : 'quattuorquinquagintillion',
	  168 : 'quinquinquagintillion',
	  171 : 'sexquinquagintillion',
	  174 : 'septenquinquagintillion',
	  177 : 'octoquinquagintillion',
	  180 : 'novemquinquagintillion',
	  183 : 'sexagintillion',
	  186 : 'unsexagintillion',
	  189 : 'duosexagintillion',
	  192 : 'tresexagintillion',
	  195 : 'quattuorsexagintillion',
	  198 : 'quinsexagintillion',
	  201 : 'sexsexagintillion',
	  204 : 'septensexagintillion',
	  207 : 'octosexagintillion',
	  210 : 'novemsexagintillion',
	  213 : 'septuagintillion',
	  216 : 'unseptuagintillion',
	  219 : 'duoseptuagintillion',
	  222 : 'treseptuagintillion',
	  225 : 'quattuorseptuagintillion',
	  228 : 'quinseptuagintillion',
	  231 : 'sexseptuagintillion',
	  234 : 'septenseptuagintillion',
	  237 : 'octoseptuagintillion',
	  240 : 'novemseptuagintillion',
	  243 : 'octogintillion',
	  246 : 'unoctogintillion',
	  249 : 'duooctogintillion',
	  252 : 'treoctogintillion',
	  255 : 'quattuoroctogintillion',
	  258 : 'quinoctogintillion',
	  261 : 'sexoctogintillion',
	  264 : 'septoctogintillion',
	  267 : 'octooctogintillion',
	  270 : 'novemoctogintillion',
	  273 : 'nonagintillion',
	  276 : 'unnonagintillion',
	  279 : 'duononagintillion',
	  282 : 'trenonagintillion',
	  285 : 'quattuornonagintillion',
	  288 : 'quinnonagintillion',
	  291 : 'sexnonagintillion',
	  294 : 'septennonagintillion',
	  297 : 'octononagintillion',
	  300 : 'novemnonagintillion',
	  303 : 'centillion'
};

function solve (input) {
	/* Deal with some special cases */
	input = input.replace(/\s/g, '');
	if (!/^(-?\d+(\.\d+)?)$/.test(input))
		return false; // malformed number, or blank (no non-whitespace chars)
	if (/^(-?0+(\.0+)?)$/.test(input))
		return 'zero';
	// remove leading and trailing zeros
	input = input.replace(/^0+([1-9]|\d\.)/, '$1'); // leading zeros
	input = input.replace(/^-0+([1-9]|\d\.)/, '-$1');// leading zeros (negative)
	input = input.replace(/(\.\d*[1-9])0+$/, '$1'); // trailing zeros
	input = input.replace(/\.0+$/, '');
	
	/* Split number into sign, integer (yxz.) and decimal (.abc) parts */
	var sign = input[0] == '-' ? 'minus ' : '';
	var integer = (/-?(\d+)/g).exec(input)[1];
	var decimal = (/-?\d+\.(\d+)/g).exec(input);
	    decimal = decimal ? decimal[1] : ''; // IFF the above statement
						 // matched, set decimal to the capturing group
	
	/* Word the integer part */
	integer = posIntegerToWords(integer);
	/* Word the decimal part */
	// build the '-ths' (e.g. 'tenths', 'hundredths', etc) decimal part
	var plural = /^0*1$/.test(decimal) ? '' : 's';
	var exp = decimal.length;
	var n = '1' + Array(exp + 1).join('0'); // number of the form 1, 10, 100, 1000, etc
	    n = posIntegerToWords(n);
	var umpths = n;
	    umpths = umpths.replace(/^one /, ''); // e.g. 'one hundred thousand' -> 'hundred thousand'
	    umpths = umpths.replace(/\s/, '-'); // e.g. 'hundred thousand' -> 'hundred-thousand'
	    umpths+= 'th' + plural;				// e.g. 'hundred thousand' -> 'hundred-thousandths'
	// build the non-'-ths' decimal part
	if (decimal != '')
		decimal = posIntegerToWords(decimal);
	else
		decimal = 'zero';
	
	/* Build the final phrase */
	var ret = sign;
	if (integer == 'zero')
		ret += decimal + ' ' + umpths;
	else if (decimal == 'zero')
		ret += integer
	else
		ret += integer + ' and ' + decimal + ' ' + umpths;
	// case where integer and decimal == zero handled above
		
	return ret;
}

function posIntegerToWords (string) {
	/* DEAL WITH NUMBERS 0 THROUGH 999 */
	if (string.length == 3) {
		// isolate digits
		var head = string[0];
		var tail = string.slice(1);
		// word the parts
		head = posIntegerToWords(head) + ' hundred'; // head.
		tail = posIntegerToWords(tail);				 // tail...
		if (tail == 'zero')
			tail = '';
		else
			tail = ' ' + tail;
		
		// combine the worded parts
		return head + tail
	}
	if (string.length == 2) {
		// deal with numbers 00 through 19 inclusive
		if (string[0] == '0')
			string = string[1];
		var num = isKnownNumber(string);
		if (num !== false)
			return num;
		// if it's not isKnownNumber it must be between 20-99 inclusive
		var umpty = isKnownNumber(string[0] + 'x');// avoid phrases like thirty-zero
		var x = isKnownNumber(string[1]);
		    x = x == 'zero' ? '' : '-' + x;
		return umpty + x;
	}
	if (string.length == 1) {
		return isKnownNumber(string);
	}
	
	/* DEAL WITH NUMBERS >= 1000 */
	var segments = [];					// break number into blocks
	while (string.length > exponentInterval) {
		var s = string.slice(- exponentInterval);
		segments.push(s);
		string = string.slice(0, - exponentInterval);
	}
	if (string != '') segments.push(string);
	var ret = '';						// build string
	for (var i = segments.length - 1; i >= 0; i--) {
		if (segments[i] == '000') continue;
		ret += solve(segments[i]);
		ret += ' ';
		ret += exponent[i*exponentInterval]; // can sometimes be a space, leading to a trailing ' , ' in ret
		ret += ', ';
	}
	return ret.replace(/[ ,]+$/, '');
}

function isKnownNumber (string) {
	for (n in number)
		if (string == n)
			return number[n];
	return false;
}

/*
	HANDLING THE FRONT-END USING JQUERY
*/
var firstClick = true;
$(document).ready(function() {
	$("input[type=button]").click(function() {
		calculate();
		firstClick = false;
	});
	bindKeyPresses();
	$("#words-container #input").click(function() {
		if (firstClick === true) {
			$("#words-container #input").val("");
			$("#words-container #result").val("");
			firstClick = false;
		}
	});
	$("#words-container #result").click(function() {
		if (firstClick === true) {
			$("#words-container #input").val("");
			$("#words-container #result").val("");
			firstClick = false;
		}
	});
});

/* Causes the input boxes to respond appropriately when
   input is typed into them */
function bindKeyPresses () {
	$("#words-container #input").keypress(function(e) {
		if(e.which == 13)
			calculate();
	});
	$("#words-container #input").keyup(function() {
		validateInput();
	});
}

/* What do do when the user clicks the "calculate"
   button. Validates input and displays a result or
   error. */
function calculate () {
	var input = getInput();
	var valid = validateInput();
	
	if (!valid) {
		setOutput(valid);
	} else {
		setOutput(solve(input));
	}
}

/* Gets the raw values from the input boxes */
function getInput () {
	return $("#words-container #input").val();
}

/* Styles the input boxes to indicate whether or not
   the input is valid. Returns true if the input is
   valid, false otherwise. */
function validateInput () {
	var isValid = true;
	
	$("#words-container #input").each(function(index) {
        $(this).removeClass("invalid");
    });
	
	$("#words-container #input").each(function(index) {
        var inputBoxContents = $(this).val();
		if (!/^(\s*(-)?\s*(\d+(\.\d+)?)?\s*)$/.test(inputBoxContents)) {
			$(this).addClass("invalid");
			isValid = false;
		}
    });
	
	return isValid;
}

function setOutput (solution) {
	if (solution === false) {
		var msg = "This doesn't look like a number to me.";
		$("#result").val(msg);
		return;
	}
	
	$("#result").val(solution);
}

/* UTILITY FUNCTIONS */

Array.prototype.remove = function(needle) {
	var haystack = this;
	for (var i = haystack.length-1; i >= 0; i--)
		if (haystack[i] === needle)
			haystack.splice(i, 1);
	return haystack;
}
