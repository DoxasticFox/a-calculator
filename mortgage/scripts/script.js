/*
	NUMBER OF COMPOUND PERIODS PER YEAR
*/
var MONTHLY 	= 12
var FORTNIGHTLY = 26
var WEEKLY		= 52

/*
	FORMULAS IN CODE FORM
*/
function downPayment (loan, repayment, term, rate, compoundPeriod) {
	loan = parseFloat(loan);
	repayment = parseFloat(repayment);
	term = parseFloat(term);
	rate = parseFloat(rate);
	
	compoundPeriod = eval(compoundPeriod);
	var i = rate/(compoundPeriod*100);
	var numCompounds = compoundPeriod*term;
	var result = loan - repayment*(Math.pow(1+i, numCompounds) - 1)/(i*Math.pow(1+i, numCompounds));
	
	if (!isFinite(result))
		return false;
	return result;
}

function loan (repayment, downPayment, term, rate, compoundPeriod) {
	repayment = parseFloat(repayment);
	downPayment = parseFloat(downPayment);
	term = parseFloat(term);
	rate = parseFloat(rate);
	
	compoundPeriod = eval(compoundPeriod);
	var i = rate/(compoundPeriod*100);
	var numCompounds = compoundPeriod*term;
	var result = downPayment + repayment*(Math.pow(1+i, numCompounds) - 1)/(i*Math.pow(1+i, numCompounds));
	
	if (!isFinite(result))
		return false;
	return result;
}

function repayment (loan, downPayment, term, rate, compoundPeriod) {
	loan = parseFloat(loan);
	downPayment = parseFloat(downPayment);
	term = parseFloat(term);
	rate = parseFloat(rate);
	
	compoundPeriod = eval(compoundPeriod);
	var i = rate/(compoundPeriod*100);
	var numCompounds = compoundPeriod*term;
	var result = (loan - downPayment)*i*Math.pow(1+i, numCompounds)/(Math.pow(1+i, numCompounds) - 1);
	
	if (!isFinite(result))
		return false;
	return result;
}

function term (loan, repayment, downPayment, rate, compoundPeriod) {
	loan = parseFloat(loan);
	repayment = parseFloat(repayment);
	downPayment = parseFloat(downPayment);
	rate = parseFloat(rate);
	
	compoundPeriod = eval(compoundPeriod);
	var i = rate/(compoundPeriod*100);
	var numCompounds = Math.log(repayment/(i*downPayment - i*loan + repayment))/Math.log(1+i);
	var result = numCompounds/compoundPeriod;
	
	if (!isFinite(result))
		return false;
	return result;
}

// Requires the rate be between 0% and 100% in order not to fail.
// On the up side, failure is graceful (false is returned).
function rate (loan, repayment, downPayment_, term, compoundPeriod) {
	loan = parseFloat(loan);
	repayment = parseFloat(repayment);
	downPayment_ = parseFloat(downPayment_);
	term = parseFloat(term);
	
	// These special cases cannot be calculated by the usual method
	// and must used be given here.
	if (loan == 0 && repayment == 0 && downPayment_ == 0)
		return 0;
	if (term == 0)
		return 0;
	
	// Usual method (approximation algorithm)
	compoundPeriod = eval(compoundPeriod);
	var lower = 0.0;
	var upper = 100.0;
	var rateEstimate = (lower + upper)/2;
	var downPaymentEst = downPayment(loan,
		repayment,
		term,
		rateEstimate,
		compoundPeriod);
	
	var count = 0;
	var maxCount = 100;
	var minAccuracy = 1e-5;
	while (Math.abs(downPayment_ - downPaymentEst) > minAccuracy && count < maxCount) {
		if (downPaymentEst < downPayment_)
			lower = rateEstimate;
		if (downPaymentEst > downPayment_)
			upper = rateEstimate;
			
		rateEstimate = (lower + upper)/2;
		downPaymentEst = downPayment(loan,
			repayment,
			term,
			rateEstimate,
			compoundPeriod);
			
		count++;
	}
	
	if (count == maxCount)
		return false;
	if (!isFinite(rateEstimate))
		return false;
	
	return rateEstimate;
}

/*
	INTERACTION WITH HTML
*/
function validateNumericalEvent (elName, event) {
	var elValue = document.getElementById(elName).value;
	var input = charFromEvent(event);
	
	if (/[^0-9\.\x08\x7F]/.test(input))
		return false;
	if (/[\.]/.test(input) && /[\.]/.test(elValue))
		return false;
	
	validateNumerical(elName);	// Just here to add/remove the 'invalid' css style
	return true;
}

function validateNumerical (elName) {
	var elValue = document.getElementById(elName).value;
	var disabled = document.getElementById(elName).disabled;
	
	if (elValue == '') {
		removeClassByName(elName, 'invalid');
		return true;
	}
	
	if (!disabled && /[^0-9.]/.test(elValue)) {
		addClassByName(elName, 'invalid');
		return false;
	}
	if (!disabled && elValue.match(/\./g) > 1) {
		addClassByName(elName, 'invalid');
		return false;
	}
	
	removeClassByName(elName, 'invalid');
	return true;
}

function validate () {
	var valid = 1;
	valid &= validateNumerical("loan") || document.getElementById("loan").disabled;
	valid &= validateNumerical("rate") || document.getElementById("rate").disabled;
	valid &= validateNumerical("term") || document.getElementById("term").disabled;
	valid &= validateNumerical("down") || document.getElementById("down").disabled;
	valid &= validateNumerical("repayment") || document.getElementById("repayment").disabled;
	if (valid != 1) {
		alert("Values must be non-negative numbers.");
		return false;
	}
	
	return true;
}

function calculate () {
	// Ensure valid input
	var valid = validate();
	if (!valid)
		return false;
	
	// Get input
	var loan_ = document.getElementById("loan").value;
	var rate_ = document.getElementById("rate").value;
	var term_ = document.getElementById("term").value;
	var down_ = document.getElementById("down").value;
	var repayment_ = document.getElementById("repayment").value;
	var compoundPeriod_ = getDropdownMenuValue("compounded");
	
	// Cannonicalize input
	if (loan_ == '')
		loan_ = '0';
	if (rate_ == '')
		rate_ = '0';
	if (term_ == '')
		term_ = '0';
	if (down_ == '')
		down_ = '0';
	if (repayment_ == '')
		repayment_ = '0';
	compoundPeriod_ = compoundPeriod_.toUpperCase();
	
	// Calculate and output
	var selected = getDropdownMenuValue("to-calculate");
	switch (selected) {
		case 'Loan Amount':
			loan_ = loan(repayment_, down_, term_, rate_, compoundPeriod_);
			break;
		case 'Interest Rate':
			rate_ = rate(loan_, repayment_, down_, term_, compoundPeriod_);
			break;
		case 'Term':
			term_ = term(loan_, repayment_, down_, rate_, compoundPeriod_);
			break;
		case 'Down Payment':
			down_ = downPayment(loan_, repayment_, term_, rate_, compoundPeriod_);
			break;
		case 'Repayment':
			repayment_ = repayment(loan_, down_, term_, rate_, compoundPeriod_);
			break;
		default:
			alert("INTERNAL ERROR");
			return false;
	}
	output(loan_, repayment_, term_, down_, rate_);
}

	
// Setter function for textboxes
function output (loan, repayment, term, downPayment, rate) {
	if (loan === false) {
		document.getElementById("loan").value = 'Unable to calculate';
	} else {
		loan = parseFloat(loan);
		document.getElementById("loan").value = loan.toFixed(2);
	}
	
	if (repayment === false) {
		document.getElementById("repayment").value = 'Unable to calculate';
	} else {
		repayment = parseFloat(repayment);
		document.getElementById("repayment").value = repayment.toFixed(2);
	}
	
	if (term === false) {
		document.getElementById("term").value = 'Unable to calculate';
	} else {
		term = parseFloat(term);
		document.getElementById("term").value = term.toFixed(2);
	}
	
	if (downPayment === false) {
		document.getElementById("down").value = 'Unable to calculate';
	} else {
		downPayment = parseFloat(downPayment);
		document.getElementById("down").value = downPayment.toFixed(2);
	}
	
	if (rate === false) {
		document.getElementById("rate").value = 'Unable to calculate';
	} else {
		rate = parseFloat(rate);
		document.getElementById("rate").value = rate.toFixed(2);
	}
}

function toCalculateChanged () {
	document.getElementById("loan").disabled = false;
	document.getElementById("rate").disabled = false;
	document.getElementById("term").disabled = false;
	document.getElementById("down").disabled = false;
	document.getElementById("repayment").disabled = false;
	
	var selected = getDropdownMenuValue("to-calculate");
	switch (selected) {
		case 'Loan Amount':
			document.getElementById("loan").disabled = true;
			break;
		case 'Interest Rate':
			document.getElementById("rate").disabled = true;
			break;
		case 'Term':
			document.getElementById("term").disabled = true;
			break;
		case 'Down Payment':
			document.getElementById("down").disabled = true;
			break;
		case 'Repayment':
			document.getElementById("repayment").disabled = true;
			break;
		default:
			alert("INTERNAL ERROR");
	}
}

function includeDown (include) {
	if (include == false) {
		// Remove 'Down Payment' from to-calculate dropdown
		var select = document.getElementById("to-calculate");
		for (var i = 0; i < select.length; /*in body*/) {
			if (select.options[i].value == 'Down Payment')
				select.remove(i);
			else
				i++;
		}
		
		// Hide 'Down Payment' textbox from view
		document.getElementById("down").value = '';
		
		var elsDown = getElementsByClassName(document, "down");
		for (var i = 0; i < elsDown.length; i++)
			elsDown[i].style.display = 'none';
			
		var elsSpacer = getElementsByClassName(document, "spacer");
		for (var i = 0; i < elsSpacer.length; i++)
			elsSpacer[i].style.display = 'inline-block';
		
		toCalculateChanged();
		return;
	}
	
	// Add 'Down Payment' to to-calculate dropdown
	var elDown=document.createElement("option")
	var textnode=document.createTextNode("Down Payment")
	elDown.setAttribute("value", "Down Payment");
	elDown.appendChild(textnode)
	
	var select = document.getElementById("to-calculate");
	select.insertBefore(elDown, select.options[1]);
	
	// Bring 'Down Payment' textbox into view
	var elsDown = getElementsByClassName(document, "down");
	for (var i = 0; i < elsDown.length; i++)
		elsDown[i].style.display = 'inline-block';
		
	var elsSpacer = getElementsByClassName(document, "spacer");
	for (var i = 0; i < elsSpacer.length; i++)
		elsSpacer[i].style.display = 'none';
		
	toCalculateChanged();
}

/*
	A FEW GENERIC HELPER FUNCTIONS
*/
function getElementsByClassName(node,classname) {
	if (node.getElementsByClassName) { // use native implementation if available
		return node.getElementsByClassName(classname);
	} else {
		return (function getElementsByClass(searchClass,node) {
			if ( node == null )
				node = document;
			var classElements = [],
				els = node.getElementsByTagName("*"),
				elsLen = els.length,
				pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)"), i, j;

			for (i = 0, j = 0; i < elsLen; i++) {
				if ( pattern.test(els[i].className) ) {
					classElements[j] = els[i];
					j++;
				}
			}
			return classElements;
		})(classname, node);
	}
}

function addClassByName (elName, className) {
	document.getElementById(elName).className += ' ' + className;
}

function removeClassByName (elName, className) {
	var re = new RegExp('(?:^|\\s)' + className + '(?!\\S)', 'g');
	var el = document.getElementById(elName);
	el.className = el.className.replace(re, '');
}

function getDropdownMenuValue (elId) {
	var e = document.getElementById(elId);
	return e.options[e.selectedIndex].value;
}

function charFromEvent (e) {
	var keynum;

	if (window.event) { // IE					
		keynum = e.keyCode;
	} else {
		if (e.which) { // Netscape/Firefox/Opera					
			keynum = e.which;
		}
	}
	
	if (typeof keynum === 'undefined')
		return 0;
	return String.fromCharCode(keynum);
}