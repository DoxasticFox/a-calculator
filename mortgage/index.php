<?php include("../include/sections.php") ?>
<?php top("Mortgage Calculator", "An online mortgage calculator for amortizing loans compounded monthly, fortnightly or weekly.") ?>
		<div id="mortgage-container">
			<div id="mortgage">
				<div class="pane">
					<label>Value to Calculate:</label>
					<select id="to-calculate" name="to-calculate" onchange="toCalculateChanged()">
						<option value="Loan Amount">Loan Amount</option>
						<option value="Term">Term</option>
						<option value="Interest Rate">Interest Rate</option>
						<option value="Repayment" selected="selected">Repayment</option>
					</select><br />
					
					<label>Include Down Payment: </label>
					<div id="radios" name="radios">
						<input type="radio" id="yes" name="down-radio" onclick="includeDown(true)"/>
						<label for="yes" onclick="includeDown(true)">Yes</label>
						
						<input type="radio" id="no" name="down-radio" checked="checked" onclick="includeDown(false)"/>
						<label for="no" onclick="includeDown(false)">No</label>
					</div>
				</div>
				
				<div class="pane">
					<label>Loan Amount: </label>
					<input id="loan" type="text" name="loan" onkeypress="return validateNumericalEvent('loan', event)" /><br />
					
					<label class="down">Down Payment: </label>
					<input id="down" class="down" type="text" name="down" onkeypress="return validateNumericalEvent('down', event)" /><br class="down" />
					
					<label>Term (years): </label>
					<input id="term" type="text" name="term" onkeypress="return validateNumericalEvent('term', event)" /><br />
					
					<label>Interest Rate (%): </label>
					<input id="rate" type="text" name="rate" onkeypress="return validateNumericalEvent('rate', event)" /><br />
					
					<label>Repayment: </label>
					<input id="repayment" type="text" name="repayment" disabled="disabled" onkeypress="return validateNumericalEvent('repayment', event)" /><br />
					
					<label>Compounded: </label>
					<select id="compounded" name="compounded">
						<option value="Weekly">Weekly</option>
						<option value="Fortnightly">Fortnightly</option>
						<option value="Monthly" selected="selected">Monthly</option>
					</select><br />
					
					<label class="spacer">Hidden: </label>
					<input id="hidden" class="spacer" type="text" name="hidden" readonly="readonly" disabled="disabled"/>
				</div>
				
				<div class="button-pane">
					<input type="button" name="calculate" value="Calculate" onclick="calculate()" />
					<div class="branding">A-CALCULATOR.COM</div>
				</div>
			</div>
		</div>
<?php middle() ?>
		<h2>FAQs &amp; How-to's</h2>
		<div id="page-text">
			<h3>About This Calculator</h3>
			<!-- FAQ Questions -->
			<ol>
				<li>
					<a href="#faq-whatis">What is this calculator for?</a>
				</li>
				<li>
					<a href="#faq-use">Can I embed this on my website?</a>
				</li>
				<li>
					<a href="#faq-formula">How can I do the math by hand?</a>
				</li>
			</ol>
			<br />
			
			<!-- FAQ Answer -->
			<h4 id="faq-whatis">
				What is this calculator for?
			</h4>
			<p>
				This mortgage calculator can be used to find the monthly repayment for an amortizing loan, as well as the term, rate, down payment and principal (loan amount).
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-use">
				Can I embed this on my website?
			</h4>
			<p>
				Sure. Embedding is allowed as long as you promise to follow <a href="/terms.html#embed">our conditions</a>. Here's the embed code:
			</p>
			<code id="embed">
				&lt;iframe width=&quot;415&quot; height=&quot;345&quot; src=&quot;http://www.a-calculator.com/mortgage/embed.html&quot; frameborder=&quot;0&quot; allowtransparency=&quot;true&quot;&gt;&lt;/iframe&gt;
			</code>
			
			<!-- FAQ Answer -->
			<h4 id="faq-formula">
				How can I do the math by hand?
			</h4>
			<p>
				The calculations are based on this formula:
				\begin{equation*}
					M = (L-D) \times \frac{i \times (1+i)^n}{(1+i)^n - 1}.
				\end{equation*}
			</p>
			<p>
				Where
				\begin{align*}
					M &= \text{Monthly repayment,} \\
					L &= \text{Loan amount (or principal),} \\
					D &= \text{Down payment,} \\
					i &= \frac{\text{Interest rate}}{\text{Number of compounding periods per year}}
				\end{align*}
				and
				\begin{align*}
					n &= \text{Total number of compounding periods.}
				\end{align*}
			</p>
			<p>
				As an example, to find the monthly repayment for a $250,000 loan at 7.5% over 25 years, you would do the following:
				\begin{align*}
					L &= 250,000 \\
					D &= 0 \\
					i &= 0.075 \div 12 = 0.00625 \qquad \text{(Compounded twelve times a year)} \\
					n &= 25 \times 12
				\end{align*}
				Then:
				\begin{align*}
					M &= (250,000 - 0) \times \frac{0.00625 \times (1 + 0.00625)^{300}}{(1 + 0.00625)^{300} - 1} \\
					M &= 1847.48
				\end{align*}
			</p>
		</div>
<?php bottom() ?>
