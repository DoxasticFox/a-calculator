function solve (input) {
	var input = input.replace(/\s/g, "");
	input = input.split(/,/);
	while (-1 != (input.indexOf("")))    // remove empty strings
		input.splice(input.indexOf(""));
	
	if (input.length == 0) return 0;
	var sum = 0;
	for (var i = 0; i < input.length; i++)
		sum += parseFloat(input[i]);
	
	return sum/input.length;
}

/*
	HANDLING THE FRONT-END USING JQUERY
*/
var firstClick = true;
$(document).ready(function() {
	$("input[type=button]").click(function() {
		calculate();
		firstClick = false;
	});
	bindKeyPresses();
	$("#average-container textarea").click(function() {
		if (firstClick === true) {
			$("#average-container textarea").val("");
			$("#result").html("Average = ?");
			firstClick = false;
		}
	});
});

/* Causes the input boxes to respond appropriately when
   input is typed into them */
function bindKeyPresses () {
	$("#average-container textarea").keypress(function(e) {
		if(e.which == 13)
			calculate();
	});
	$("#average-container textarea").keyup(function() {
		validateInput();
	});
}

/* What do do when the user clicks the "calculate"
   button. Validates input and displays a result or
   error. */
function calculate () {
	var input = getInput();
	var valid = validateInput();
	
	if (!valid) {
		setOutput(valid);
	} else {
		setOutput(solve(input));
	}
}

/* Gets the raw values from the input boxes */
function getInput () {
	return $("#average-container textarea").val();
}

/* Styles the input boxes to indicate whether or not
   the input is valid. Returns true if the input is
   valid, false otherwise. */
function validateInput () {
	var isValid = true;
	
	$("#average-container textarea").each(function(index) {
        $(this).removeClass("invalid");
    });
	
	$("#average-container textarea").each(function(index) {
		/* Construct regular expression */
		var comma = "(\\s*,\\s*)";
		var sign = "(\\s*(-|\\+)\\s*)";
		var number = "(\\s*" + sign + "?[0-9]+(\\.[0-9]+)*\\s*)"; // number with optional sign
		var head = "(" + number + comma + "?)"; // optionally signed number with optional trailing comma
		var tail = "((" + number + comma + ")+)" // comma-separated numbers with trailing comma
		var empty = "(^$|\\s*)"; // The null string (or whitespace)
        var or = "|";
        var re = empty;
			re+= or + head;
			re+= or + "(" + tail + head + ")";
            re = "^(" + re + ")$"; 			// The textbox should contain the only the pattern
        
        /* jquery stuff */
        var re = new RegExp(re);
        var inputBoxContents = $(this).val();
		if (!re.test(inputBoxContents)) {
			$(this).addClass("invalid");
			isValid = false;
		}
    });
	
	return isValid;
}

function setOutput (solution) {
	if (solution === false) {
		var msg = "You must enter numbers separated by commas!";
		$("#result").html(msg);
		return;
	}
	
	var sign = solution < 0 ? "&minus;" : "";
	$("#result").html("Average = " + sign + Math.abs(solution));
}

/* UTILITY FUNCTIONS */

Array.prototype.remove = function(needle) {
	var haystack = this;
	for (var i = haystack.length-1; i >= 0; i--)
		if (haystack[i] === needle)
			haystack.splice(i, 1);
	return haystack;
}
