<?php include("../include/sections.php") ?>
<?php top("Average Calculator", "Finds the average (or mean) of a list of numbers.") ?>
		<div id="average-container">
			<div id="average">
				<div class="pane">
					<label>Numbers:</label>
					<textarea rows="4" cols="34">1, -42, 5</textarea>
					<br />
					<br />
					<label id="result">Average = &minus;12</label>
				</div>
				
				<div class="button-pane">
					<input type="button" name="calculate" value="Calculate" />
					<div class="branding">A-CALCULATOR.COM</div>
				</div>
			</div>
		</div>
<?php middle() ?>
		<h2>FAQs &amp; How-to's</h2>
		<div id="page-text">
			<h3>About This Calculator</h3>
			<!-- FAQ Questions -->
			<ol>
				<li>
					<a href="#faq-whatis">What is this calculator for?</a>
				</li>
				<li>
					<a href="#faq-use">Can I embed this on my website?</a>
				</li>
				<li>
					<a href="#faq-formula">How can I find the average manually?</a>
				</li>
			</ol>
			<br />
			
			<!-- FAQ Answer -->
			<h4 id="faq-whatis">
				What is this calculator for?
			</h4>
			<p>
				This calculator finds the average (arithmetic mean) of a collection of numbers.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-use">
				Can I embed this on my website?
			</h4>
			<p>
				Sure. Embedding is allowed as long as you promise to follow <a href="/terms.html#embed">our conditions</a>. Here's the embed code:
			</p>
			<code id="embed">
				&lt;iframe width=&quot;415&quot; height=&quot;220&quot; src=&quot;http://www.a-calculator.com/average/embed.html&quot; frameborder=&quot;0&quot; allowtransparency=&quot;true&quot;&gt;&lt;/iframe&gt;
			</code>
			
			<!-- FAQ Answer -->
			<h4 id="faq-formula">
				How can I find the average manually?
			</h4>
			<p>
				To take the average of a group of numbers, you simply sum it then divide by the size of the group. For example, there are 3 numbers in the list 1, -42, 5, so the average is:
				\begin{equation}
					\frac{1 -42 + 5}{3} = -12.
				\end{equation}
				In mathematical notation the arithmetic mean can be written as:
				\begin{equation}
					\frac{1}{n}\sum\limits_{i = 0}^n a_i = \frac{1}{n}(a_1 + a_2 + \dots + a_n).
				\end{equation}
 			</p>
		</div>
<?php bottom() ?>
