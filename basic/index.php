<?php include("../include/sections.php") ?>
<?php top("Simple Online Calculator", "A simple online calculator. Allows embedding and includes frequently asked questions on the memory and percentage buttons.") ?>
		<div id="calculator-container">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td id="display" colspan="6">0</td>
				</tr>
				<tr>
					<td colspan="6" class="zero-height">&nbsp;</td>
				</tr>
				<tr id="memsrow">
					<td></td>
					<td>
						<input class="mems" type="button" value="MC" onclick="memoryButton('MC'); " />
					</td>
					<td>
						<input class="mems" type="button" value="MR" onclick="memoryButton('MR');" />
					</td>
					<td>
						<input class="mems" type="button" value="MS" onclick="memoryButton('MS');" />
					</td>
					<td>
						<input class="mems" type="button" value="M+" onclick="memoryButton('MP');" />
					</td>
					<td>
						<input class="mems" type="button" value="M&minus;" onclick="memoryButton('MM');" />
					</td>
				</tr>
				<tr>
					<td>
						<input class="ops" type="button" value="&plusmn;" onclick="operatorButton('negate');" />
					</td>
					<td>
						<input class="nums" type="button" value="7" onclick="numberButton(7); " />
					</td>
					<td>
						<input class="nums" type="button" value="8" onclick="numberButton(8);" />
					</td>
					<td>
						<input class="nums" type="button" value="9" onclick="numberButton(9);" />
					</td>
					<td>
						<input class="ops" type="button" value="%" onclick="operatorButton('percent');" />
					</td>
					<td>
						<input class="ops" type="button" value="&#8730;" onclick="operatorButton('sqrt');" />
					</td>
				</tr>
				<tr>
					<td>
						<input class="ops pi" type="button" value="&pi;" onclick="numberButton('pi');" />
					</td>
					<td>
						<input class="nums" type="button" value="4" onclick="numberButton(4);" />
					</td>
					<td>
						<input class="nums" type="button" value="5" onclick="numberButton(5);" />
					</td>
					<td>
						<input class="nums" type="button" value="6" onclick="numberButton(6);" />
					</td>
					<td>
						<input class="ops" type="button" value="&#215;" onclick="operatorButton('multiply');" />
					</td>
					<td>
						<input class="ops" type="button" value="&#247;" onclick="operatorButton('divide');" />
					</td>
				</tr>
				<tr>
					<td rowspan="2">
						<input id="clear" class="tall-button" type="button" value="C" onclick="reset();" />
					</td>
					<td>
						<input class="nums" type="button" value="1" onclick="numberButton(1);" />
					</td>
					<td>
						<input class="nums" type="button" value="2" onclick="numberButton(2);" />
					</td>
					<td>
						<input class="nums" type="button" value="3" onclick="numberButton(3);" />
					</td>
					<td rowspan="2">
						<input class="ops tall-button" type="button" value="+" onclick="operatorButton('add');" />
					</td>
					<td>
						<input class="ops" type="button" value="&minus;" onclick="operatorButton('subtract');" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input class="nums wide-button" type="button" value="0" onclick="numberButton(0);" />
					</td>
					<td>
						<input class="nums" type="button" value="." onclick="numberButton('.');" />
					</td>
					<td>
						<input class="ops" type="button" value="=" onclick="operatorButton('equals');" />
					</td>
				</tr>
				<tr>
					<th class="branding" colspan="6">A-CALCULATOR.COM</th>
				</tr>
			</table>
		</div>
<?php middle() ?>
		<h2>FAQs &amp; How-to's</h2>
		<div id="page-text">
			<!-- FAQ Questions -->
			<h3>How To Use</h3>
			<ol>
				<li>
					<a href="#faq-whatis">What is <em>A Calculator</em>?</a>
				</li>
				<li>
					<a href="#faq-mobile">Can I use <em>A Calculator</em> with my iPod, iPhone or Android device?</a>
				</li>
				<li>
					<a href="#faq-use">Can I embed this on my website?</a>
				</li>
				<li>
					<a href="#faq-mem">How do I use the memory buttons?</a>
				</li>
				<li>
					<a href="#faq-negate">How do I enter negative numbers?</a>
				</li>
				<li>
					<a href="#faq-percent">How do I use the percent (%) button?</a>
				</li>
				<li>
					<a href="#faq-unexpected-ans">If I enter 2&#215;&minus;3= I get &minus;1. Shouldn't I get &minus;6?</a>
				</li>
			</ol>
			<h3>Math Advice</h3>
			<ol>
				<li>
					<a href="#faq-expression">What is an expression?</a>
				</li>
				<li>
					<a href="#faq-operand">What is an operand?</a>
				</li>
				<li>
					<a href="#faq-operator">What is an operator?</a>
				</li>
				<li>
					<a href="#faq-round">I receive a long answer. How do I round it to the nearest integer or decimal place?</a>
				</li>
			</ol>
			<br />
			
			<!-- FAQ Answer -->
			<h4 id="faq-whatis">
				What is <em>A Calculator</em>?
			</h4>
			<p>
				<em>A Calculator</em> is our flagship offering &mdash; a free online calculator designed to work like its handheld, electronic counterparts. It can store numbers in memory and calculate percentages. It also includes pi and a square root button.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-mobile">
				Can I use <em>A Calculator</em> with my iPad, iPhone or Android device?
			</h4>
			<p>
				Sure. Unlike Flash-based alternatives, <em>A Calculator</em> works with iPhone and iPad. In fact, it should work with any Android browser that supports JavaScript.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-use">
				Can I embed this on my website?
			</h4>
			<p>
				Yep! You can use <em>A Calculator</em> for your website as long as you promise to follow <a href="/terms.html#embed">our conditions</a>. Here's the embed code:
			</p>
			<code id="embed">
				&lt;iframe width=&quot;315&quot; height=&quot;340&quot; src=&quot;http://www.a-calculator.com/basic/embed.html&quot; frameborder=&quot;0&quot; allowtransparency=&quot;true&quot;&gt;&lt;/iframe&gt;
			</code>
			
			<!-- FAQ Answer -->
			<h4 id="faq-mem">
				How do I use the memory buttons?
			</h4>
			<p>
				Here's a list of what the memory buttons do:
			</p>
			<ul>
				<li>
					<span class="mems">MC</span> &mdash; Short for "memory clear". Sets the memory to zero.
				</li>
				<li>
					<span class="mems">MR</span> &mdash; "Memory recall". Retrieves the stored number and displays it onscreen.
				</li>
				<li>
					<span class="mems">MS</span> &mdash; "Memory store". Takes the numeral displayed onscreen and stores it. Anything that used to be in memory is erased by the new number.
				</li>
				<li>
					<span class="mems">M+</span> &mdash; Adds the onscreen numeral to memory.
				</li>
				<li>
					<span class="mems">M&minus;</span> &mdash; Subtracts the displayed numeral from memory.
				</li>
			</ul>
			<p>
				To test these buttons, you can enter a number and press <span class="mems">MS</span>. It's now stored.
			</p>
			<p>
				Now you can perform your calculations as usual and even reset the calculator using <span class="clear">C</span>. When you're ready to use the stored number, just press <span class="mems">MR</span> and it'll be recalled.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-negate">
				How do I enter negative numbers?
			</h4>
			<p>
				If you want to perform an <a href="#faq-operator">operation</a> on negative numbers, you should use the <span class="ops">&plusmn;</span> button. For instance, if you want to divide &minus;42 by &minus;2, you would simply type <span class="nums">4</span> <span class="nums">2</span> <span class="ops">&plusmn;</span> <span class="ops">&#247;</span> <span class="nums">2</span> <span class="ops">&plusmn;</span> <span class="ops">=</span>. If you typed <span class="ops">&minus;</span> <span class="nums">4</span> <span class="nums">2</span> <span class="ops">&#247;</span> <span class="ops">&minus;</span> <span class="nums">2</span> <span class="ops">=</span>, you would receive an incorrect answer. More information about this can be found <a href="#faq-unexpected-ans">here</a>.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-percent">
				How do I use the percent (%) button?
			</h4>
			<p>
				The percent button can be used to find a percentage of another number.
			</p>
			<p>
				Say you want to add 33 to 26 percent of 33. Then you would type <span class="nums">3</span> <span class="nums">3</span> <span class="ops">+</span> <span class="nums">2</span> <span class="nums">6</span> <span class="ops">%</span>. At this point 8.58 (which is 26 percent of 33) should appear onscreen. To add this to 33, simply press <span class="ops">=</span>.
			</p>
			<p>
				Similarly, to divide 89 by 56 percent of 89, for instance, you would type <span class="nums">8</span> <span class="nums">9</span> <span class="ops">&#247;</span> <span class="nums">5</span> <span class="nums">6</span> <span class="ops">%</span> <span class="ops">=</span>.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-unexpected-ans">
				If I enter 2&#215;&minus;3= I get &minus;1. Shouldn't I get &minus;6?
			</h4>
			<p>
				Just as you would expect from most electronic calculators, only the most recently pressed <a href="#faq-operator">operator</a> button, <span class="ops">&minus;</span>, is used. For example, if you typed in <span class="nums">2</span> <span class="ops">&#215;</span> <span class="ops">&minus;</span> <span class="ops">&#247;</span> <span class="ops">&#247;</span> <span class="ops">+</span> <span class="nums">3</span> <span class="ops">=</span> the result would be the same as if you entered <span class="nums">2</span> <span class="ops">+</span> <span class="nums">3</span> <span class="ops">=</span>.
			</p>
			<p>
				If you want to perform an operation on negative <a href="#faq-operand">operands</a>, you should use the <span class="ops">&plusmn;</span> button which negates the displayed numeral. For example, to find 2 times &minus;3, you would enter <span class="nums">2</span> <span class="ops">&#215;</span> <span class="nums">3</span> <span class="ops">&plusmn;</span> <span class="ops">=</span>.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-expression">
				What is an expression?
			</h4>
			<p>
				An expression in math is like a sentence in English.
			</p>
			<p>
				An expression is just a combination of symbols that have a meaning or value. It can contain numerals, operators and other symbols. For example, 2+2.5 and 2&#247;9+&#8730;<span class="overline">4</span> are both mathematical expressions.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-operand">
				What is an operand?
			</h4>
			<p>
				An operand is a symbol which is affected by an operator. Two, nine and four in the expression 2+&#8730;<span class="overline">9</span>&#247;4 are all operands.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-operator">
				What is an operator?
			</h4>
			<p>
				An operator is a symbol which effects one or more operands. In the expression, 2+2.5, the addition operator (+) operates on 2 and 2.5 to produce 4.5.
			</p>
			
			<!-- FAQ Answer -->
			<h4 id="faq-round">
				I receive a long answer. How do I round it to the nearest whole or decimal place?
			</h4>
			<p>
				The purpose of rounding is to make a number easier to work with. The problem is that by rounding you lose some accuracy. To make sure you lose as little accuracy as possible, you should choose the number which is closest to the original (unrounded) one. Take the number 7.1 as an example. To round it to the nearest whole, you'd simply choose 7 because 7 is closer to 7.1 than 8 is.
			</p>
			<p>
				Rounding a 23.5 is tricky, because it's just as close to 23 as it is to 24. There are a few different <a href="http://www.mathsisfun.com/numbers/rounding-methods.html" rel="nofollow">methods</a> for dealing with this case, though the most commonly used is to round up. In this case, 23.5 rounds up to 24.
			</p>
		</div>
<?php bottom() ?>
