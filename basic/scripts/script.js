$(document).keydown(function(event) {
	sendEvent(event);
});

var expression = [null, null, null];
var pointer = 0;
var memory = "0";
var memoryRead = false;
var error = false;
var timeout = null;
var pi = "3.14159265358979";

function reset () {
	expression = [null, null, null];
	pointer = 0;
	error = false;
	memoryRead = false;
	
	refreshDisplay();
	displayFlash();
}

function sendEvent (event) {
	var charCode = event.keyCode;
	var numPadOffset = 48;
	
	switch (charCode)
	{
		/* Operations, etc */
		case 13:
			operatorButton('equals');
			break;
		case 27:
			reset();
			break;
		case 42:
		case 106:
			operatorButton('multiply');
			break;
		case 43:
		case 107:
			operatorButton('add');
			break;
		case 45:
		case 109:
			operatorButton('subtract');
			break;
		case 46:
		case 110:
			numberButton('.');
			break;
		case 47:
		case 111:
			operatorButton('divide');
			break;
		
		/* Numbers */
		case 96:		// numpad 0
		case 97:
		case 98:
		case 99:
		case 100:
		case 101:
		case 102:
		case 103:
		case 104:
		case 105:		// numpad 9
			numberButton(String.fromCharCode(charCode-numPadOffset));
			break;
		case 48:		// 0
		case 49:
		case 50:
		case 51:
		case 52:
		case 53:
		case 54:
		case 55:
		case 56:
		case 57:		// 9
			numberButton(String.fromCharCode(charCode));
			break;
	}
}

function hasPoint (pointer) {
	if (typeof expression[pointer] == "string")
		return (expression[pointer].indexOf(".") !== -1);
	else
		return false;
}

function appendNumber (pointer, input) {
	if (error)
		return;
	
	if (expression[pointer] == null)
		expression[pointer] = "0";
	
	if (/[^0-9-\.]/.test(expression[pointer]))
		return expression[pointer].toString();
	
	if (/[^0-9-\.]/.test(input))
		return expression[pointer].toString();
	
	if (expression[pointer].replace(/(^-)|\./,"").length >= 15)
		return expression[pointer].toString();
		
	if (expression[pointer] == "0") {
		if (input == "0")
			return "0";
		
		if (input == ".")
			return "0.";
			
		return input.toString();
	}
	
	if (!(input == "." && hasPoint(pointer)))
		return expression[pointer] + input;
	else
		return expression[pointer].toString();
}

function memoryButton(input) {	
	if (error)
		return;
		
	if (input == "MC") {
		memory = "0";
		refreshDisplay();
		displayFlash();
		return;
	}
	
	if (input == "MR") {
		if (pointer == 0 || pointer == 3) {
			expression[0] = memory;
			refreshDisplay();
			displayFlash();
			memoryRead = true;
			return;
		}
		
		if (pointer == 2) {
			expression[2] = memory;
			refreshDisplay();
			displayFlash();
			memoryRead = true;
			return;
		}
	}
	
	if (input == "MS") {
		if (pointer == 0 || pointer == 3)
			memory = cleanEval(expression[0]);
		
		if (pointer == 2)
			memory = cleanEval(expression[2]);
			
		if (memory === null)
			memory = "0";
			
		refreshDisplay();
		displayFlash();
		memoryRead = true;
		return;
	}
	
	if (input == "MP") {
		if (pointer == 0 || pointer == 3) {
			if (cleanEval(memory + "+" + expression[0]) !== false)
				memory = cleanEval(memory + "+" + expression[0]);
			else 
				error = true;
		}
		
		if (pointer == 2) {
			if (cleanEval(memory + "-" + expression[2]) !== false)
				memory = cleanEval(memory + "-" + expression[2]);
			else 
				error = true;
		}
			
		refreshDisplay();
		displayFlash();
		memoryRead = true;
		return;
	}
	
	if (input == "MM") {	
		if (pointer == 0 || pointer == 3) {
			if (cleanEval(memory + "-" + expression[0]) !== false)
				memory = cleanEval(memory + "-" + expression[0]);
			else 
				error = true;
		}
		
		if (pointer == 2) {
			if (cleanEval(memory + "-" + expression[2]) !== false)
				memory = cleanEval(memory + "-" + expression[2]);
			else 
				error = true;
		}
			
		refreshDisplay();
		displayFlash();
		memoryRead = true;
		return;
	}
}

function numberButton (input) {
	if (error)
		return;
	
	// pi stuff
	if (input == "pi") {
		if (pointer == 0 || pointer == 3) {
			expression[0] = pi;
			refreshDisplay();
			displayFlash();
			return;
		}
		
		if (pointer == 2) {
			expression[2] = pi;
			refreshDisplay();
			displayFlash();
			return;
		}
	}
	
	input = input.toString();
	
	if (pointer == 3) {
		expression[0] = null;
		pointer = 0;
	}
	
	if (pointer == 0) {
		expression[1] = null;
		expression[2] = null;
	}
	
	if (memoryRead) {
		if (pointer == 3)
			expression[0] = null;
		else
			expression[pointer] = null;
			
		memoryRead = false;
	}
	
	expression[pointer] = appendNumber(pointer, input);
	display(expression[pointer]);
	displayFlash();
}

function operatorButton (input) {
	if (error)
		return;
	
	input = input.toString();
	
	switch (input)
	{
		case "percent":
			if (pointer == 2) {
				if (expression[2] == null)
					expression[2] = expression[0];
				
				if (cleanEval(expression[0]*expression[2]/100) === false)
					error = true;
				
				expression[2] = cleanEval(expression[0]*expression[2]/100);
				display(expression[2]);
				break;
			}
			
			if (cleanEval(expression[0]/100) === false)
				error = true;
			expression[0] = cleanEval(expression[0]/100);
			display(expression[0]);
			pointer = 3;
			break;
		case "sqrt":
			if (pointer == 2) {
				if (evaluate(expression) === false)
					error = true;
				
				if (cleanEval(Math.sqrt(evaluate(expression))) === false)
					error = true;
				
				expression[0] = cleanEval(Math.sqrt(evaluate(expression)));
				display(expression[0]);
				pointer = 3;
				break;
			}
			
			if (cleanEval(Math.sqrt(expression[0])) === false)
				error = true;
			expression[0] = cleanEval(Math.sqrt(expression[0]));
			display(expression[0]);
			pointer = 3;
			break;
		case "negate":
			if (pointer == 3) {
				if (negate(0) !== false) {
					expression[0] = negate(0);
					display(expression[0]);
				}
				break;
			}
			
			if (pointer == 2 && expression[pointer] == null) {
				if (negate(pointer) !== false) {
					expression[pointer] = negate(0);
					display(expression[pointer]);
				}
				break;
			}
			
			if (negate(pointer) !== false) {
				expression[pointer] = negate(pointer);
				display(expression[pointer]);
			}
			break;
		case "add":
		case "subtract":
		case "multiply":
		case "divide":
			if (pointer == 2 && expression[2] != null) {
				if (evaluate(expression) === false)
					error = true;
				expression[0] = evaluate(expression);
				display(expression[0]);
				expression[1] = input;
				expression[2] = null;
				break;
			}
			
			if (pointer == 2 && expression[2] == null) {
				expression[1] = input;
				break;
			}
			
			if (expression[0] == null)
				expression[0] = "0";
			expression[1] = input;
			expression[2] = null;
			pointer = 2;
			break;
		case "equals":
			if (evaluate(expression) === false)
				error = true;
			expression[0] = evaluate(expression);
			display(expression[0]);	
			pointer = 3;
			break;
		default:
			alert("INTERNAL ERROR: operatorButton");
			return;
	}
	
	displayFlash();
	memoryRead = false;
}

function evaluate (expression) {
	if (error)
		return false;
	
	if (expression[0] == null)
		return "0";
		
	if (expression[1] == null)
		return eval(expression[0]).toString();
	
	if (expression[2] == null)
		expression[2] = expression[0];
		
	var operator;
	switch (expression[1])
	{
		case "add":
			operator = "+";
			break;
		case "subtract":
			operator = "-";
			break;
		case "multiply":
			operator = "*";
			break;
		case "divide":
			operator = "/";
			break;
		default:
			alert("INTERNAL ERROR: evaluate");
			return false;
	}
	
	return cleanEval(expression[0] + operator + expression[2] + "");
}

function cleanEval (evalString) {
	var result = eval(evalString);
	
	if (!isFinite(result))
		return false;
	
	if (round(Math.abs(result), 0) > 1e15 - 1)
		return false;
	
	var numDigitsLeft = round(Math.abs(result), 0).toString().length;
	if (Math.abs(result) < 1 && round(Math.abs(result), 14) >= 1e-14) {
		var temp = round(result, 15-numDigitsLeft);
		temp = temp.toFixed(14);
		temp = temp.replace(/0+$/, "");
		return temp;
	} else {
		return round(result, 15-numDigitsLeft).toString();
	}
}

// adds commas to output and displays it,
// or displays error.
function display (output) {
	if (error) {
		document.getElementById("display").innerHTML = "ERROR";
		return;
	}
	
	if (output == null)
		var output = "0";
	var parts = output.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	var display = parts.join(".");
	if (memory != "0")
		display = '<div id="memory">M</div>' + display;
	
	document.getElementById("display").innerHTML = display;
}

function refreshDisplay () {
	if (pointer == 0
		|| pointer == 3
		|| (pointer == 2 && expression[2] === null)) {
		display(expression[0]);
		return;
	}
	
	if (pointer == 2) {
		display(expression[2]);
		return;
	}
}

function displayFlash () {
	if (error)
		return;
	
	var original = document.getElementById("display").innerHTML;
	document.getElementById("display").innerHTML = "&nbsp;";
	
	window.clearTimeout(timeout);
	timeout = setTimeout(function() {
		document.getElementById("display").innerHTML = original;
	}, 35);
}

function negate (pointer) {
	if (expression[pointer] == null)
		expression[pointer] = "0";
		
	if (expression[pointer] == "0")
		return expression[pointer];
	
	if (typeof expression[pointer] != "string")
		return false;
	
	if (expression[pointer][0] == "-")
		return expression[pointer].substring(1, expression[pointer].length);
	else
		return "-" + expression[pointer];
}

function round(number, digits) {
	var multiple = Math.pow(10, digits);
	var rounded = Math.round(number * multiple) / multiple;
	return rounded;
}
